/* eslint-disable import/prefer-default-export */
export const logoTechnogi = '/images/logo.png'
export const ninjaLogo = '/images/ninjalogo.png'
export const homeUrl = '/images/home.jpg'
export const aboutUrl = '/images/about.jpg'
export const talkUrl = '/images/talk.png'
export const computerUrl = '/images/computer.png'
export const ninjaPinkUrl = '/images/ninja-pink.png'
export const heartUrl = '/images/heart.png'
