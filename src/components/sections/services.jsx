/**
 * Component for page of services 2020
 *
 */
import React from 'react'
import { Hidden } from '@material-ui/core'
import { talkUrl, computerUrl } from '../../constants'
import Section from '../section'

const Services = () => (
  <React.Fragment>
    <div className="service-main">
      <div>
        <Section
          title="Digital Projects"
          body="Si tienes herramientas y recursos sin soporte técnico, nosotros nos encargamos: actualizamos <<código heredado>> y lo volvemos funcional"
          imageUrl={computerUrl}
          url="#/contact"
          linkText="Conocer servicio"
        />
      </div>
      <div>
        <Section
          title="Smart Sourcing"
          body="Diseñamos equipos a medida, ágiles y encausados a cada proyecto ante cualquier distancia existente"
          imageUrl={talkUrl}
          url="#/contact"
          linkText="Conocer servicio"
        />
      </div>
    </div>
    <style jsx>{`
        .service-main {
            width: 100%;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-flow: row wrap;
            div{
                padding: 0 2em;
                width: 50%;
            }
        }

        .service-arrow{
            font-size: 0.5em;
        }

    `}</style>
  </React.Fragment>
)

export default Services
