import Services from './services'
import Contact from './contact'
import Home from './home'
import About from './about'

export {
  Services,
  Contact,
  Home,
  About,
}
