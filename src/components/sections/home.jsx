/**
 * Component for page of services 2020
 *
 */

import React from 'react'
import { Hidden } from '@material-ui/core'
import { Card, ImageContainer } from '../commons'

import { homeUrl } from '../../constants'

const Home = () => {
  const title = 'Creamos y desarrollamos software enfocado en innovación tecnología en el ámbito de las TI.'
  const description = 'Asesoría especializada en proyectos complejos ligados a IA, AWS, migración a la nube e Internet de las cosas, entre otras...'
  const url = '#contact'
  const linkText = 'Contáctanos'

  return (
    <React.Fragment>
      <div className="home-main">
        <ImageContainer id="home" imageUrl={homeUrl}>
          <Card
            title={title}
            description={description}
            url={url}
            linkText={linkText}
          />
        </ImageContainer>
      </div>
      <style jsx>{`
        .home-main{
            width: 100%;
            height: 100vh;
            margin-top: -7em;
        }
    `}</style>
    </React.Fragment>
  )
}

export default Home
