/**
 * Component for page of services 2020
 *
 */

import React from 'react'
import { Container, Hidden } from '@material-ui/core'
import { ninjaPinkUrl, heartUrl } from '../../constants'

const Contact = () => (
  <React.Fragment>
    <Container maxWidth="lg">
      <div className="contact-main">
        <div className="contact-column contact-left">
          <div className="contact-slogan contact-color-red"> Hi, lets keep in touch.</div>
          <div>
            <img src={ninjaPinkUrl} alt={ninjaPinkUrl} className="contact-ninja"/>
          </div>
          <div className="contact-row">
            <div className="contact-column contact-phone">
              <div className="contact-title">escríbenos</div>
              <div className="contact-color-red contact-subtitle ">info@technogi.com.mx</div>
            </div>
            <div className="contact-column contact-phone">
              <div className="contact-title">llámanos</div>
              <div className="contact-color-red contact-subtitle">+525559802990</div>
            </div>
          </div>
        </div>
        <div className="contact-column contact-right">
          <div>
            <img src={heartUrl} alt={heartUrl} className="contact-heart" />
          </div>
          <div className="contact-column">
            <div className="contact-title">visítanos</div>
            <div className="contact-color-red contact-subtitle">Av. Cd Universitaria 286 - Jardines del Pedregal 01900 Ciudad de México, CDMX México</div>
          </div>
        </div>
      </div>
    </Container>
    <style jsx>{`
        .contact-main {
            width: 100%;
            height: 100vh;
            display: flex;
            justify-content: flex-start;
            align-items: center;
            flex-flow: row wrap;

            .contact-left {
                width: 70%;
            }

            .contact-color-red {
                color: #FF6668;
            }

            .contact-phone {
                padding: 1em 4em 1em 0;
            }

            .contact-title {
                font-size: 3em;
                font-weight: 500;
            }

            .contact-subtitle {
                font-size: 2em;
                font-weight: 500;
            }

            .contact-slogan{
                font-weight: 500;
                font-size: 4em;
            }
            .contact-right {
                width: 30%;
            }
            .contact-ninja {
                width: 5em;
            }
            .contact-heart {
                width: 25em;
            }
            .contact-column{
                display: flex;
                justify-content: center;
                align-items: flex-start;
                flex-flow: column wrap;
            }

            .contact-row{
                width: 100%;
                display: flex;
                justify-content: flex-start;
                align-items: flex-start;
                flex-flow: row wrap;
            }
        }
    `}</style>
  </React.Fragment>
)

export default Contact
