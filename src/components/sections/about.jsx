/**
 * Component for page of services 2020
 *
 */


import React from 'react'
import { Hidden } from '@material-ui/core'
import { Card, ImageContainer } from '../commons'

import { aboutUrl } from '../../constants'

const Home = () => {
  const title = 'La solución está ahí, pero casi nadie sabe dónde: seleccionamos y empleamos las herramientas precisas para cada proyecto,'
  const description = 'basados en el conocimiento de todo el espectro digital y tecnológico a nivel mundial'
  const url = '#contact'
  const linkText = 'Contáctanos'

  return (
    <React.Fragment>
      <div className="about-main">
        <ImageContainer id="about" imageUrl={aboutUrl}>
          <Card
            title={title}
            description={description}
            url={url}
            notSpacing
            linkText={linkText}
          />
        </ImageContainer>
      </div>
      <style jsx>{`
        .about-main{
            width: 100%;
            height: 100vh;
        }
    `}</style>
    </React.Fragment>
  )
}

export default Home
