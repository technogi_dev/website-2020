import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { TextField } from '@material-ui/core'

export default ({ label, color }) => {
  const useStyles = makeStyles({
    formControl: {
      color: `${color || '#fff'} !important`,
      borderBottom: `1px solid ${color || '#fff'}`,
    },
    input: {
      color: `${color || '#fff'}`,
    },
  })

  const classes = useStyles()
  return (
    <TextField label={label} classes={classes} />
  )
}
