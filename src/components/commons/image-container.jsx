import React from 'react'
import { Container } from '@material-ui/core'
import PropTypes from 'prop-types'

const ImageContainer = ({ id, children, imageUrl, className }) => (
  <React.Fragment>
    <div id={`section-${id}`}>
      <Container maxWidth="xl">
        <div className={`${className || 'section-inner'}`}>
          {children}
        </div>
      </Container>
    </div>
    <style jsx global>{`
        #section-${id} {
          width: 100%;
          height: 100vh;
          background-image: url(${imageUrl});
          background-repeat: no-repeat;
          background-size: cover;
          margin-bottom: 1em;    
        }  

        .section-inner{
            display: flex;
            justify-content: flex-end;
            align-items: flex-start;
            flex-flow: column wrap;
            height: 100vh;
            width: 100%;
        }
      `}</style>
          
  </React.Fragment>
)

ImageContainer.propTypes = {
  id: PropTypes.node.isRequired,
  seo: PropTypes.node.isRequired,
  imageUrl: PropTypes.node.isRequired
}

export default ImageContainer
