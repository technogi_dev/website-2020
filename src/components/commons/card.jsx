
import React from 'react'
import { Link } from 'gatsby'
import { Button, Paper } from '@material-ui/core'
import classnames from 'classnames'

export default ({ title, description, url, notSpacing, linkText }) => (
  <React.Fragment>
    {/* <div className="card-main"> */}
    <Paper square elevation={3} className="card-main">
      <h1 className={classnames({ 'card-h1': notSpacing })}>{title}</h1>
      <div className="card-description">{description}</div>
      <div>
        <Link to={url}>
          <Button variant="contained" color="primary" type="button" className="card-btn">{linkText}</Button>
        </Link>
      </div>
      </Paper>
    {/* </div> */}
    <style jsx global> {`
        .card-h1{
            margin-block-end: 0px;
        }

        .card-main {
            display: flex;
            justify-content: flex-start;
            align-items: flex-start;
            flex-flow: column wrap;
            background: #fff;
            padding: 0em 2em;
            margin-bottom: 2em;
            width: 45em;
            height: 25em;
            /* box-shadow: 0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12); */
            .card-description {
                font-size: 1.6em;
                padding-bottom: 1em;
            }
        }
        .card-btn{
            color: #fff !important;
            padding: 1em 5em !important;
        }
    `}</style>
  </React.Fragment>
)
