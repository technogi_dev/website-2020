import React from 'react'
import { Button } from '@material-ui/core'
import { ninjaLogo } from '../constants'
import { TextField } from './commons/forms'

export default () => (
  <form noValidate className="form-sing-up-main">
    <div>
      <img src={ninjaLogo} alt={ninjaLogo} className="img-ninja" />
    </div>
    <div>
      <h2>JOIN OUR NEWSLETTER</h2>
    </div>
    <div>
      <TextField label="Enter your email"/>
    </div>
    <div>
      <Button variant="contained" color="default" className="form-sing-up-btn-send">Sing Up</Button>
    </div>
    <style jsx global>{`
        .form-sing-up-main{
            display: flex;
            width: 100%;
            justify-content: center;
            align-items: center;
            flex-flow: column wrap;
            margin-top: 15em;
            margin-bottom: 2em;
            div {
                text-align: center;
                width: 100%;
            }
            .img-ninja {
                width: 5em;
            }

            .form-sing-up-btn-send{
                width: 100%;
                margin: 1em 0.5em 0;
            }
            .form-sing-up-input{
                color: #fff !important;
            }
        }
    `}</style>
  </form>
)

