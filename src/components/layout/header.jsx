import React from 'react'
import { AppBar, Toolbar, Typography, Hidden } from '@material-ui/core'
import { Link } from 'gatsby'
import classnames from 'classnames'
import { connect } from 'react-redux'
import MenuIcon from '@material-ui/icons/Menu'

import { logoTechnogi } from '../../constants'

const Header = ({ activeTab }) => (
  <header>
    <AppBar position="fixed" color="secondary" className="nav">
      <Toolbar>
        <Link to="#/home">
          <img src={logoTechnogi} alt={logoTechnogi} className="img-logo" />
        </Link>
        <div className="nav-inner">
          <Hidden smUp>
            <MenuIcon />
          </Hidden>
          <Hidden smDown>
            <Link to="#home"><Typography className={classnames('nav-item', { 'nav-color-inactive': activeTab !== 'home' }, { 'nav-color-active': activeTab === 'home' })}>Home</Typography></Link>
            <Link to="#about"><Typography className={classnames('nav-item', { 'nav-color-inactive': activeTab !== 'about' }, { 'nav-color-active': activeTab === 'about' })}>About</Typography></Link>
            <Link to="#services"><Typography className={classnames('nav-item', { 'nav-color-inactive': activeTab !== 'services' }, { 'nav-color-active': activeTab === 'services' })}>Services</Typography></Link>
            <Link to="#contact"><Typography className={classnames('nav-item', { 'nav-color-inactive': activeTab !== 'contact' }, { 'nav-color-active': activeTab === 'contact' })}>Contact</Typography></Link>
          </Hidden>
        </div>
      </Toolbar>
    </AppBar>
    <style jsx global>{`
          .img-logo{
            padding: 0.5em;
            width: 25em;

            @media screen and (max-width: 375px){
              width: 15em;
            }
          }
  
          .nav {
            margin-top: 2em;
            height: 5em; 
          }
  
          .nav-inner {
            display: flex;
            justify-content: flex-end;
            align-items: center;
            width: 100%;
          }
  
          .nav-item{
            padding: 0 1em !important;
            font-weight: bold !important;
          }
          .nav-color-inactive{
            color: #fff !important;
          } 
          .nav-color-active {
            color: #FF6668 !important;
          }
          
        `}</style>
  </header>
)

const mapStateToProps = (state) => ({
  activeTab: state.home.activeTab,
})

export default connect(mapStateToProps, {})(Header)
