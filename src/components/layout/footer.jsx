import React from 'react'
import { Container } from '@material-ui/core'
import { logoTechnogi } from '../../constants'
import FormNewsLetter from '../form-newsletter'

const Footer = () => (
  <footer>
    <div className="footer">
      <Container maxWidth="xl">
        <img src={logoTechnogi} alt={logoTechnogi} className="img-logo" />
        <hr />
        <div className="footer-inner">
          <div className="footer-left">
            <h2>CONTACT US</h2>
            <div className="footer-content-section">
                info@technogi.com.mx
            </div>
            <h2>
                FOLLOW US
            </h2>
            <div className="footer-content-section">
              <div>Facebook</div>
              <div>Twitter</div>
              <div>Instagram</div>
              <div>Linkedin</div>
            </div>
            <h2>
              FIND US
            </h2>
            <div className="footer-content-section">
              <div>Mexico City</div>
              <div>London</div>
            </div>
          </div>
          <div className="footer-right">
            <FormNewsLetter />
          </div>
        </div>
      </Container>
    </div>
    <style jsx>{`
        .footer {
          background-color: #333333;
          color: #fff;
          height: 100%
        }
        
        .footer-inner {
          width: 100%;
          display: flex;
          justify-content: flex-start;
          align-items: center;
          flex-flow: row wrap;
        }

        .footer-content-section {
          font-weight: 500;
          font-size: 1.6em;
        }
        .footer-left {
          width: 70%;
        }

        .footer-right {
          width: 30%;
          display: flex;
          justify-content: center;
          align-items: center;
          flex-flow: column wrap;
        }

        hr {
          background: #fff;
        }

      `}</style>
  </footer>
)

export default Footer
