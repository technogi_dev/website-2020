/**
 * Component for page of services 2020
 *
 */
import React from 'react'
import { Link } from 'gatsby'
import PropTypes from 'prop-types'

const Section = ({ title, body, imageUrl, url, linkText }) => (
  <React.Fragment>
    <div className="section-main" id="services">
      <h2 className="section-title">{title}</h2>
      <div>
        <img src={imageUrl} alt={imageUrl} className="section-img" />
      </div>
      <div className="section-body">{body}</div>
      <div>
        <Link to={url}>
          <span className="section-link">{linkText}</span>
        </Link>
      </div>
    </div>
    <style jsx global>{`
        .section-main{
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: flex-start;
            align-items: flex-start;
            flex-flow: column wrap;
            
            .section-title{
                color: #FF6668;
                text-transform: uppercase;
            }

            .section-img{
                width: 100%;
            }

            .section-body{
                font-weight: 500;
                font-size: 1.5em;
                padding: 1em 0 2em;
            }

            .section-link{
                text-transform: uppercase;
                color: #FF6668 !important;
                padding: 2em 0 0.5em;
                border-bottom: 1px solid #FF6668;
                font-size: bold !important;
            }
        }
   `}</style>
  </React.Fragment>
    
)

Section.propTypes = {
  title: PropTypes.node.isRequired,
  body: PropTypes.node.isRequired,
  imageUrl: PropTypes.node.isRequired,
  url: PropTypes.node.isRequired,
  linkText: PropTypes.node.isRequired,
}

export default Section
