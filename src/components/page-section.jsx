
import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Seo } from './commons'

const PageSection = ({ seo, id, children }) => (
  <Fragment>
    <Seo title={seo} />
    <div id={id} className="page-section">
      {children}
    </div>
    <style jsx global>{`
       .page-section {
          height: 100vh;
          width: 100%;
          margin-bottom: 1em;
       }
    `}</style>
  </Fragment>
)

PageSection.propTypes = {
  id: PropTypes.node.isRequired,
  seo: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
}

export default PageSection
