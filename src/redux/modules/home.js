
const PERCENTAGE_SCROLLED = 'home:percentage_scrolled'
const INITIAL_STATE = { percentageScrolled: undefined, activeTab: 'home' }

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case PERCENTAGE_SCROLLED:
      return {
        ...state,
        percentageScrolled: action.payload.percentage,
        activeTab: action.payload.activeTab,
      }
    default:
      return state
  }
}

export const setPercentageScrolled = (percentage) => {
  let activeTab

  if (percentage >= 0 && percentage <= 20) activeTab = 'home'
  if (percentage >= 20 && percentage <= 40) activeTab = 'about'
  if (percentage >= 40 && percentage <= 60) activeTab = 'services'
  if (percentage >= 60) activeTab = 'contact'
      
  return ({
    type: PERCENTAGE_SCROLLED,
    payload: { percentage, activeTab },
  })
}
