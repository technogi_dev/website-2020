/**
 * © Derechos Reservados 2020 Technogi Todos los Derechos Reservados.
 *
 * La reproducción de este archivo queda estrictamente prohibida.
 *
 * @summary Creation of root reducer redux
 * @author JACG <josealberto.cano@technogi.com.mx>
 *
 * Created at     : 2020-06-03 14:00:00
 * Last modified  : 2020-06-03 14:05:54
 *
 */

import { combineReducers } from 'redux'
import homeReducer from './home'

export default combineReducers({
  home: homeReducer,
})

