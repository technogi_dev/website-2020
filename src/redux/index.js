/**
 * long description for the file
 *
 * @summary short description for the file
 * @author jacg <josealberto.cano@technogi.com.mx>
 *
 * Created at     : 2018-06-18 14:38:50
 * Last modified  : 2018-06-18 14:50:41
 */

import { createStore, applyMiddleware } from 'redux'

import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import reducers from './modules'

const isProduction = process.env.NODE_ENV === 'production'
const initialState = {}

export default function makeStore() {
  let store
  
  if (isProduction) {
    store = createStore(reducers, initialState, applyMiddleware(
      thunk,
    ))
  }

  store = createStore(
    reducers,
    initialState,
    composeWithDevTools(applyMiddleware(
      thunk,
    )),
  )
  
  return store
}
