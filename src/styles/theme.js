import { createMuiTheme } from '@material-ui/core/styles'
import { akzidenzGroteskRegular, akzidenzGroteskBold, akzidenzGroteskMedium, akzidenzGroteskLight } from '../../static/fonts'

export default createMuiTheme({
  typography: {
    fontFamily: [
      'AkzidenzGrotesk',
    ],
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [akzidenzGroteskRegular, akzidenzGroteskBold, akzidenzGroteskMedium, akzidenzGroteskLight],
      },
    },
  },
  palette: {
    primary: {
      main: '#FF6668',
    },
    secondary: {
      main: '#333333',
    },
    info: {
      main: '#4C73DB',
    },
    default: {
      main: '#FFF',
    },
  },
  status: {
    danger: 'orange',
  },
})
  
