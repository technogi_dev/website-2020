/* eslint-disable import/prefer-default-export */
const getDocHeight = (documentElement, body) => Math.max(
  body.scrollHeight,
  documentElement.scrollHeight,
  body.offsetHeight,
  documentElement.offsetHeight,
  body.clientHeight,
  documentElement.clientHeight,
)
  
export const percentageScrolledListener = (htmlDocument, window) => {
  if (htmlDocument && window) {
    const { innerHeight, pageYOffset } = window
    const { documentElement, body } = htmlDocument
  
    const winheight = innerHeight || (documentElement || body).clientHeight
    const docheight = getDocHeight(documentElement, body)
    const scrollTop = pageYOffset || (documentElement || body.parentNode || body).scrollTop
    const trackLength = docheight - winheight
  
    return Math.floor((scrollTop / trackLength) * 100)
  }
  
  return null
}
  
