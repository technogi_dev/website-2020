import React from 'react'
import { connect } from 'react-redux'

import { Layout } from '../components/layout'
import { PageSection } from '../components'
import { Services, Contact, Home, About } from '../components/sections'

// import { Parallax } from 'react-scroll-parallax'

import  { percentageScrolledListener } from '../utils'
import  { setPercentageScrolled } from '../redux/modules/home'


class IndexPage extends React.Component {

  constructor(props) {
    super(props)
    this.handleScroll = this.handleScroll.bind(this)
  }

  componentDidMount(){
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll() {
    this.props.setPercentageScrolled(percentageScrolledListener(document, window))
  }
  
  render(){
    return (
      <Layout>
          <PageSection id="home" seo="Home">
            <Home />
          </PageSection>
          <PageSection id="about" seo="About" >
            <About />
          </PageSection>
          <PageSection seo="Services" id="services">
            <Services />
          </PageSection>
          <PageSection seo="Contact" id="contact">
            <Contact />
          </PageSection> 
      </Layout>
    )
  }
}

// const mapStateToProps = state =>({
  
// })

export default connect(null, { setPercentageScrolled })(IndexPage)
