import React from 'react'
import { Link } from 'gatsby'
import { Layout } from '../components/layout'
import { Seo } from '../components/commons'

const NotFoundPage = () => (
  <Layout>
    <Seo title="404: Not found" />
    <h1>Página no encontrada</h1>
    <p>La Página que esta buscando no existe, contacte con el administrador</p>
    <Link to="/">Home</Link>
  </Layout>
)

export default NotFoundPage
