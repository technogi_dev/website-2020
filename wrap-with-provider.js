import React from 'react'
import { Provider } from 'react-redux'

import makeStore from './src/redux'

export default ({ element }) => {
  const store = makeStore()
  return <Provider store= {store}>{element}</Provider>
}
