import AkzidenzGrotesk from './AkzidenzGrotesk-Regular.otf'
import AkzidenzGroteskBold from './AkzidenzGrotesk-Bold.otf'
import AkzidenzGroteskMedium from './AkzidenzGrotesk-Medium.otf'
import AkzidenzGroteskLight from './AkzidenzGrotesk-Light.otf'

const akzidenzGroteskRegular = {
  fontFamily: 'AkzidenzGrotesk',
  fontStyle: 'normal',
  
  src: `
    local('AkzidenzGrotesk'),
    local('AkzidenzGrotesk-Regular'),
    url(${AkzidenzGrotesk}) format('woff2')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
}
const akzidenzGroteskBold = {
  fontFamily: 'AkzidenzGrotesk',
  fontStyle: 'normal',
  fontWeight: 'bold',
  src: `
    local('AkzidenzGrotesk'),
    local('AkzidenzGrotesk-Bold'),
    url(${AkzidenzGroteskBold}) format('woff2')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
}
const akzidenzGroteskMedium = {
  fontFamily: 'AkzidenzGrotesk',
  fontStyle: 'normal',
  fontWeight: '500',
  src: `
    local('AkzidenzGrotesk'),
    local('AkzidenzGrotesk-Medium'),
    url(${AkzidenzGroteskMedium}) format('woff2')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
}

const akzidenzGroteskLight = {
  fontFamily: 'AkzidenzGrotesk',
  fontStyle: 'normal',
  fontWeight: 'lighter',
  src: `
    local('AkzidenzGrotesk'),
    local('AkzidenzGrotesk-Light'),
    url(${AkzidenzGroteskLight}) format('woff2')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
}

export {
  akzidenzGroteskLight,
  akzidenzGroteskMedium,
  akzidenzGroteskBold,
  akzidenzGroteskRegular,
}
